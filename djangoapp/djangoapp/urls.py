"""djangoapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from api.views import ContactGenericViewSet,ContactViewSet, GenericApiView,ContactDetails,ContactApiView,contact_details_api_view,contact_list,contact_details,contact_api_view
from rest_framework.routers import DefaultRouter

router1  = DefaultRouter()
router1.register('contact',ContactViewSet,basename='contact viewset')
router2  = DefaultRouter()
router2.register('contact',ContactGenericViewSet,basename='contact viewset')
urlpatterns = [
    path('viewset/',include(router1.urls)),
    path('viewset/<name>',include(router1.urls)),
    path('genericviewset/',include(router2.urls)),
    path('admin/', admin.site.urls),
    path('',contact_list),
    path('contact_details/<int:pk>/',contact_details),
    path('contact_api_view/',contact_api_view),
    path('contact_details_api_view/<int:pk>/',contact_details_api_view),
    path('Contact_class_view/',ContactApiView.as_view()),
    path('ContactDetails/<name>',ContactDetails.as_view()),
    path('generic/contact/',GenericApiView.as_view()),
    path('generic/contact/<name>',GenericApiView.as_view())
]
