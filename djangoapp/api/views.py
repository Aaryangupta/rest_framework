from django.shortcuts import render
from django.http import HttpResponse,JsonResponse
from rest_framework.parsers import JSONParser
from .models import Contact
from .serializers import ContactModelSerializer,ContactSerializer
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
# Create your views here.
from rest_framework.views import APIView

from rest_framework import generics,mixins,viewsets
#authentication 
from rest_framework.authentication import TokenAuthentication,SessionAuthentication,BaseAuthentication,BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from django.shortcuts import get_list_or_404
# generic viewset api
class ContactGenericViewSet(viewsets.GenericViewSet,mixins.DestroyModelMixin,mixins.RetrieveModelMixin,mixins.UpdateModelMixin,mixins.CreateModelMixin,mixins.ListModelMixin):
    serializer_class = ContactSerializer
    queryset  = Contact.objects.all()



#viewset api

class ContactViewSet(viewsets.ViewSet):
    def get_object(self,name):
        try:
            return Contact.objects.get(name=name)
        except Contact.DoesNotExist:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)
    def list(self, request):
        contact = Contact.objects.all()
        serializer = ContactSerializer(contact,many = True)
        return Response(serializer.data)

    def create(self, request):
        serializer = ContactSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)


    def retrieve(self, request, name=None):
        contact = self.get_object(name)
        # queryser = Contact.objects.all()
        # contact = get_list_or_404(queryser,name = name)
        serializer = ContactSerializer(contact)
        return Response(serializer.data)

    def update(self, request,  name=None):
        contact = self.get_object(name)
        serializer = ContactSerializer(contact,data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

    def partial_update(self, request, name=None):
        pass

    def destroy(self, request, name=None):
        contact = self.get_object(name)
        contact.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)



#generic api views
class GenericApiView(generics.GenericAPIView,mixins.ListModelMixin,mixins.CreateModelMixin,mixins.UpdateModelMixin,mixins.RetrieveModelMixin,mixins.DestroyModelMixin):
    serializer_class = ContactSerializer
    queryset = Contact.objects.all()
    lookup_field = 'name' 

    # authentication_classes = [SessionAuthentication,BasicAuthentication]
    authentication_classes = [TokenAuthentication] 
    permission_classes = [IsAuthenticated]

    def get(self,request,name=None):
        if name:
            return self.retrieve(request)
        else:
            return self.list(request)
    def post(self,request):
        return self.create(request)
    def put(self,request,name = None):
        return self.update(request,name)
    def delete(self,request,name):
        return self.destroy(request,name)

#Class based view 
class ContactApiView(APIView):
    def get(self,request):
        contact = Contact.objects.all()
        serializer = ContactSerializer(contact,many = True)
        return Response(serializer.data)
    def post(self,request):
        serializer = ContactSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)
    def delete(self,request):
        contact.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ContactDetails(APIView):
    def get_object(self,name):
        try:
            return Contact.objects.get(name=name)
        except Contact.DoesNotExist:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    def get(self,request,name):
        contact = self.get_object(name)
        serializer = ContactSerializer(contact)
        return Response(serializer.data)
    
    def put(self,request,name):
        contact = self.get_object(name)
        serializer = ContactSerializer(contact,data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
    def delete(self,request,name):
        contact = self.get_object(name)
        contact.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

















#Function base view
@api_view(['GET','POST'])
def contact_api_view(request):

    if request.method == 'GET':
        contact = Contact.objects.all()
        serializer = ContactSerializer(contact,many = True)
        return Response(serializer.data)
    elif request.method == 'POST':
        serializer = ContactSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)


@api_view(['GET','PUT','DELETE'])
def contact_details_api_view(request,pk):
    try:
        contact = Contact.objects.get(pk=pk)
    except:
        return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    if request.method =='GET':
        serializer = ContactSerializer(contact)
        return Response(serializer.data)
    elif request.method =='PUT':
        serializer = ContactSerializer(contact,data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
    elif request.method =='DELETE':
        contact.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

@csrf_exempt
def contact_list(request):

    if request.method == 'GET':
        contact = Contact.objects.all()
        serializer = ContactSerializer(contact,many = True)
        return JsonResponse(serializer.data,safe=False)
    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = ContactSerializer(data = data)

        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data,status = 201)
        return JsonResponse(serializer.errors,status = 400)
@csrf_exempt
def contact_details(request,pk):
    try:
        contact = Contact.objects.get(pk = pk)
    except:
        return HttpResponse(status=404)

    if request.method =='GET':
        serializer = ContactSerializer(contact)
        return JsonResponse(serializer.data)
    elif request.method =='PUT':
        data = JSONParser().parse(contact)
        serializer = ContactSerializer(contact,data = data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors,status = 400)
    elif request.method =='DELETE':
        contact.delete()
        return HttpResponse(status=204)




