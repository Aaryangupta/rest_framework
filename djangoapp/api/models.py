from django.db import models

# Create your models here.
class Contact(models.Model):
    name = models.CharField(max_length=200)
    Surname = models.CharField(max_length=200)
    date= models.DateField()

    def __str__(self):
        return self.name

class Details(models.Model):
    ids = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    Surname = models.CharField(max_length=200)
    
    def __str__(self):
        return self.name
