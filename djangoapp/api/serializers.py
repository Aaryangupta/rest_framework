from rest_framework import serializers
from .models import Contact

class ContactSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=200)
    Surname = serializers.CharField(max_length=200)
    date= serializers.DateField()

    def create(self,validated_data):
        return Contact.objects.create(**validated_data)

    def update(self,instance,validated_data):
        instance.name = validated_data.get('name',instance.name)
        instance.Surname = validated_data.get('Surname',instance.Surname)
        instance.date = validated_data.get('date',instance.date)
        instance.save()
        return instance


class ContactModelSerializer(serializers.ModelSerializer):
    class Meta:
        model= Contact
        fields = ['name','phone','email']



    